default:
	run

# Start the containers
run:
	docker-compose -p skinka-test-task up -d

# Stop the containers
stop:
	docker-compose -p skinka-test-task down

# Install environments and run project
install:
	docker-compose -p skinka-test-task up -d --build
	docker-compose -p skinka-test-task exec php composer install --no-interaction --ansi
	docker-compose -p skinka-test-task exec php php -r "file_exists('.env') || copy('.env.example', '.env');"
	docker-compose -p skinka-test-task exec php php bin/console app:db:init
	docker-compose -p skinka-test-task exec php php bin/console app:db:seed
	docker-compose -p skinka-test-task exec php php bin/console app:user:add user 123456

# Test
test:
	docker-compose -p skinka-test-task exec php php vendor/bin/phpunit
