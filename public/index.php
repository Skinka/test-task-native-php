<?php

require_once(__DIR__.'/../vendor/autoload.php');

$dotenv = new \Symfony\Component\Dotenv\Dotenv();
$dotenv->usePutenv(true)->bootEnv(__DIR__.'/../.env');

$conf = require(__DIR__.'/../config/app.php');

$app = new \App\Application($conf);
$app->run();
