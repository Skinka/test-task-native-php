<?php


namespace App\System;

use App\Entities\User;
use Jasny\Auth\ContextInterface;
use Jasny\Auth\StorageInterface;
use Jasny\Auth\UserInterface;

class AuthStorage implements StorageInterface
{
    protected ?\PDO $db;

    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->db = App::$app->db;
    }

    /**
     * Fetch a user by ID
     */
    public function fetchUserById(string $id): ?UserInterface
    {
        $stmt = $this->db->prepare("SELECT * FROM users WHERE id = ?");
        $stmt->execute([$id]);
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        if ($data) {
            $data['hashedPassword'] = $data['password'];
            return User::fromData($data);
        }
        return null;
    }

    /**
     * Fetch a user by username
     */
    public function fetchUserByUsername(string $username): ?UserInterface
    {
        $stmt = $this->db->prepare("SELECT * FROM users WHERE username = ?");
        $stmt->execute([$username]);
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);

        if ($data) {
            $data['hashedPassword'] = $data['password'];
            return User::fromData($data);
        }
        return null;
    }

    /**
     * Fetch the context by ID.
     */
    public function fetchContext(string $id): ?ContextInterface
    {
        return null;
    }

    /**
     * Get the default context of the user.
     */
    public function getContextForUser(UserInterface $user): ?ContextInterface
    {
        return null;
    }
}
