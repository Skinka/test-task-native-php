<?php


namespace App;

use App\Commands\DeliverMoneyPrizesCommand;
use App\Commands\InitDatabaseCommand;
use App\Commands\SeedDataCommand;
use App\Commands\UserAddCommand;
use App\Subscribers\MoneyPrizeSubscriber;
use App\System\App;
use App\System\AuthStorage;
use Exception;
use Jasny\Auth\Auth;
use Jasny\Auth\Authz\Levels;
use PDO;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Application as ConsoleApp;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader as YamlFileLoaderDi;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\Routing\Loader\YamlFileLoader as YamlFileLoaderRoute;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;

/**
 * Class Application
 * @package Application
 */
class Application
{
    public const APP_WEB     = 'web';
    public const APP_CONSOLE = 'console';
    public const APP_TEST    = 'test';

    public ?PDO             $db;
    public ContainerBuilder $di;
    public ?Auth            $auth;
    public array            $config;

    protected string $appType;

    public function __construct(array $config)
    {
        App::$app = $this;
        $this->config = $config;
    }

    /**
     * Загрузка приложения
     * @throws Exception
     */
    public function run()
    {
        $this->appType = self::APP_WEB;
        $this->container();
        $this->subscribeListeners();
        $this->database();
        $this->auth();
        $this->http();
    }

    /**
     * Загрузка приложения
     * @throws Exception
     */
    public function runTest()
    {
        $this->appType = self::APP_TEST;
        $this->container();
        $this->subscribeListeners();
        $this->db = null;
        $this->auth = null;
    }

    /**
     * Загрузка консоли
     * @throws Exception
     */
    public function runConsole()
    {
        $this->appType = self::APP_CONSOLE;
        $application = new ConsoleApp();
        $this->container();
        $this->subscribeListeners();
        $this->database();
        $application->addCommands(
            [
                new InitDatabaseCommand(),
                new SeedDataCommand(),
                new UserAddCommand(),
                new DeliverMoneyPrizesCommand(),
            ]
        );
        $application->run();
    }

    public function getDispatcher(): EventDispatcher
    {
        /** @var EventDispatcher $dispatcher */
        $dispatcher = $this->di->get('dispatcher');
        return $dispatcher;
    }

    /**
     * @throws Exception
     */
    protected function database()
    {
        $this->db = new PDO(
            sprintf(
                'mysql:host=%s;dbname=%s',
                $this->config['db']['host'],
                $this->config['db']['database'],
            ),
            $this->config['db']['user'],
            $this->config['db']['password']
        );

        if ($this->db->errorCode()) {
            $err = $this->db->errorInfo();
            throw new Exception(
                sprintf(
                    'Database connect error (%s-%d): %s',
                    $err[0],
                    $err[1],
                    $err[2],
                )
            );
        }
    }

    /**
     * @throws Exception
     */
    protected function container()
    {
        $this->di = new ContainerBuilder();
        $loader = new YamlFileLoaderDi($this->di, new FileLocator(__DIR__.'/../config'));
        $loader->load('di.yaml');
    }

    /**
     * @throws Exception
     */
    protected function subscribeListeners()
    {
        $dispatcher = $this->getDispatcher();
        $dispatcher->addSubscriber($this->di->get(MoneyPrizeSubscriber::class));
    }

    /**
     * @throws Exception
     */
    protected function http()
    {
        $fileLocator = new FileLocator([__DIR__.'/../config']);
        $loader = new YamlFileLoaderRoute($fileLocator);
        $routes = $loader->load('routes.yaml');

        $request = Request::createFromGlobals();

        $matcher = new UrlMatcher($routes, new RequestContext());

        $dispatcher = $this->getDispatcher();
        $dispatcher->addSubscriber(new RouterListener($matcher, new RequestStack()));

        $controllerResolver = new ControllerResolver();
        $argumentResolver = new ArgumentResolver();

        $kernel = new HttpKernel($dispatcher, $controllerResolver, new RequestStack(), $argumentResolver);

        $response = $kernel->handle($request);
        $response->send();
    }

    protected function auth()
    {
        $levels = new Levels(['user' => 1]);
        $auth = new Auth($levels, new AuthStorage());

        session_start();
        $auth->initialize();
        $this->auth = $auth;
    }
}
