<?php


namespace App\Commands;

use App\Components\BankInterface;
use App\Repositories\PrizeRepository;
use App\System\App;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DeliverPrizesCommand
 * @package App\Commands
 */
class DeliverMoneyPrizesCommand extends BaseCommand
{
    public BankInterface $bankApi;

    /**
     * @throws Exception
     */
    public function __construct(string $name = null)
    {
        parent::__construct($name);

        $this->bankApi = App::$app->di->get(BankInterface::class);
    }

    protected static $defaultName = 'app:prizes:money-delivery';

    protected function configure(): void
    {
        $this->setDescription('Delivering users prizes')
            ->addArgument('batchItems', InputArgument::REQUIRED, 'Amount transactions in batch.');
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $batchAmount = $input->getArgument('batchItems');
        $prizeRepo = App::$app->di->get(PrizeRepository::class);

        $count = $prizeRepo->getCountNotDeliveredMoney();
        $pages = ceil($count / $batchAmount);
        $lastId = 0;
        for ($page = 0; $page < $pages; $page++) {
            $data = $prizeRepo->getNotDeliveredMoney($lastId, $batchAmount);
            foreach ($data as $item) {
                $this->bankApi->sendTransaction($item['id'], $item['amount'], $item['bank_data']);
            }
            $lastId = end($data)['id'];
        }
        return Command::SUCCESS;
    }
}
