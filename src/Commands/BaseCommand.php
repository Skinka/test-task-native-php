<?php


namespace App\Commands;

use Symfony\Component\Console\Command\Command;

/**
 * Class BaseCommand
 * @package App\Commands
 */
abstract class BaseCommand extends Command
{

}
