<?php


namespace App\Commands;

use App\System\App;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SeedDataCommand
 * @package App\Commands
 */
class SeedDataCommand extends BaseCommand
{
    protected static $defaultName = 'app:db:seed';

    protected function configure(): void
    {
        $this->setDescription('Seed Data in Database');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        for ($i = 0; $i < rand(3, 5); $i++) {
            $amount = rand(10, 50);
            $product = sprintf('Product #%d', $i);
            $query = App::$app->db->prepare(
                <<<'SQL'
                INSERT INTO `things` (`name`, `amount`) value (:name, :amount) 
SQL
            );
            $query->bindParam(':name', $product);
            $query->bindParam(':amount', $amount, \PDO::PARAM_INT);
            $query->execute();
        }
        for ($i = 0; $i < rand(3, 5); $i++) {
            $amount = rand(100, 5000);
            $query = App::$app->db->prepare(
                <<<'SQL'
                INSERT INTO `money` (`amount`) value (:amount) 
SQL
            );
            $query->bindParam(':amount', $amount);
            $query->execute();
        }

        $output->writeln('Data seeded');
        return Command::SUCCESS;
    }
}
