<?php


namespace App\Commands;

use App\Entities\User;
use App\Repositories\UserRepository;
use App\System\App;
use Exception;
use Faker\Factory;
use PDO;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UserAddCommand
 * @package App\Commands
 */
class UserAddCommand extends BaseCommand
{
    protected static $defaultName = 'app:user:add';

    protected function configure(): void
    {
        $this->setDescription('Add User')
            ->addArgument('username', InputArgument::REQUIRED, 'The username of the user.')
            ->addArgument('password', InputArgument::REQUIRED, 'The user password.');
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $user = new User();
        $name = $input->getArgument('username');
        if (!$this->checkUnique($name)) {
            $output->writeln(sprintf('User "%s" is exist', $name));
            return Command::FAILURE;
        }
        $user->username = (string)$name;
        $user->setPassword($input->getArgument('password'));
        $faker = Factory::create();
        $user->address = $faker->address;
        $user->bank_data = $faker->bankAccountNumber;

        $repo = App::$app->di->get(UserRepository::class);
        if ($repo->store($user)) {
            $output->writeln('User Added');
            return Command::SUCCESS;
        }
        return Command::FAILURE;
    }

    /**
     * Проверяет на уникальность имя пользователя
     *
     * @param $username
     *
     * @return bool
     */
    protected function checkUnique($username): bool
    {
        $query = App::$app->db
            ->prepare('SELECT COUNT(*) cnt FROM `users` WHERE `username` = :username');
        $query->bindParam(':username', $username);
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);
        return $data['cnt'] == 0;
    }
}
