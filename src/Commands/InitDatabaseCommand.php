<?php


namespace App\Commands;

use App\System\App;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class InitApplication
 * @package App\Commands
 */
class InitDatabaseCommand extends BaseCommand
{
    protected static $defaultName = 'app:db:init';

    protected function configure(): void
    {
        $this->setDescription('Init Database');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        App::$app->db->query(
            <<<'SQL'
            CREATE TABLE IF NOT EXISTS `users`  (
                `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
                `username` varchar(255) NOT NULL UNIQUE,
                `password` varchar(255) NOT NULL,
                `bonus_deposit` double NOT NULL DEFAULT 0,
                `address` text NOT NULL,
                `bank_data` text NOT NULL,
                PRIMARY KEY (`id`)
            );
            SQL
        );

        App::$app->db->query(
            <<<'SQL'
            CREATE TABLE IF NOT EXISTS `things`  (
                `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
                `name` varchar(255) NOT NULL,
                `amount` int NOT NULL DEFAULT 0,
                PRIMARY KEY (`id`)
            );
            SQL
        );

        App::$app->db->query(
            <<<'SQL'
            CREATE TABLE IF NOT EXISTS `money`  (
                `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
                `amount` double NOT NULL DEFAULT 0,
                PRIMARY KEY (`id`)
            );
            SQL
        );

        App::$app->db->query(
            <<<'SQL'
            CREATE TABLE IF NOT EXISTS `user_bonus_prizes`  (
                `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
                `user_id` bigint UNSIGNED NOT NULL,
                `amount` double NOT NULL DEFAULT 0,
                `delivered_at` timestamp NULL,
                `canceled_at` timestamp NULL,
                `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                CONSTRAINT `fk-user_id-user_bonus_prizes` FOREIGN KEY (`user_id`) REFERENCES `users` (id)
            );
            SQL
        );

        App::$app->db->query(
            <<<'SQL'
            CREATE TABLE IF NOT EXISTS `user_money_prizes`  (
                `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
                `user_id` bigint UNSIGNED NOT NULL,
                `amount` double NOT NULL DEFAULT 0,
                `delivered_at` timestamp NULL,
                `canceled_at` timestamp NULL,
                `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                CONSTRAINT `fk-user_id-user_money_prizes` FOREIGN KEY (`user_id`) REFERENCES `users` (id)
            );
            SQL
        );

        App::$app->db->query(
            <<<'SQL'
            CREATE TABLE IF NOT EXISTS `user_thing_prizes`  (
                `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
                `user_id` bigint UNSIGNED NOT NULL,
                `thing_id` bigint UNSIGNED NOT NULL,
                `delivered_at` timestamp NULL,
                `canceled_at` timestamp NULL,
                `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                CONSTRAINT `fk-user_id-user_thing_prizes` FOREIGN KEY (`user_id`) REFERENCES `users` (id),
                CONSTRAINT `fk-thing_id-user_thing_prizes` FOREIGN KEY (`thing_id`) REFERENCES `things` (id)
            );
            SQL
        );

        $output->writeln('Database success');
        return Command::SUCCESS;
    }
}
