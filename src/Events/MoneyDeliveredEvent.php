<?php


namespace App\Events;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class MoneyDeliveredEvent
 * @package App\Events
 */
class MoneyDeliveredEvent extends Event
{
    public const NAME = 'money-prize.delivered.event';

    private int $moneyPrizeId;

    public function __construct(int $moneyPrizeId)
    {
        $this->moneyPrizeId = $moneyPrizeId;
    }

    public function getMoneyPrizeId(): int
    {
        return $this->moneyPrizeId;
    }
}
