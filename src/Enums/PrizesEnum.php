<?php


namespace App\Enums;


class PrizesEnum
{
    public const BONUS = 'bonus';
    public const MONEY = 'money';
    public const THING = 'thing';
}
