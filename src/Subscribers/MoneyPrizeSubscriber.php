<?php


namespace App\Subscribers;

use App\Events\MoneyDeliveredEvent;
use App\Repositories\PrizeRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class MoneyPrizeDeliveredListener
 * @package App\Listeners
 */
class MoneyPrizeSubscriber implements EventSubscriberInterface
{
    private PrizeRepository $prizeRepo;

    public function __construct(PrizeRepository $prizeRepo)
    {
        $this->prizeRepo = $prizeRepo;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            MoneyDeliveredEvent::NAME => 'onDeliveredEvent'
        ];
    }

    /**
     * Приз был отправлен на счет в банке
     *
     * @param  MoneyDeliveredEvent  $event
     */
    public function onDeliveredEvent(MoneyDeliveredEvent $event)
    {
        $this->prizeRepo->deliveryMoney($event->getMoneyPrizeId());
    }
}
