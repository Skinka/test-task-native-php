<?php


namespace App\Entities;

/**
 * Class BaseEntity
 * @package App\Entities
 */
abstract class BaseEntity
{
    /**
     * @param  array  $data
     *
     * @return static
     */
    public static function fromData(array $data): self
    {
        $self = new static();

        foreach ($data as $field => $value) {
            if (property_exists($self, $field)) {
                $self->{$field} = $value;
            }
        }

        return $self;
    }
}
