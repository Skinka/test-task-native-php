<?php


namespace App\Entities;

use Jasny\Auth;

/**
 * Class User
 * @package App\Entities
 */
class User extends BaseEntity implements Auth\UserInterface
{
    public string $id;
    public string $username;
    public float  $bonus_deposit;
    public string $address;
    public string $bank_data;
    public int    $accessLevel = 0;

    public string $hashedPassword = '';

    public function getAuthId(): string
    {
        return $this->id;
    }

    public function setPassword($password)
    {
        $this->hashedPassword = password_hash($password, PASSWORD_BCRYPT);
    }

    public function verifyPassword(string $password): bool
    {
        return password_verify($password, $this->hashedPassword);
    }

    public function getAuthChecksum(): string
    {
        return hash('sha256', $this->id.$this->hashedPassword);
    }

    public function getAuthRole(Auth\ContextInterface $context = null): int
    {
        return $this->accessLevel;
    }

    public function requiresMfa(): bool
    {
        return false;
    }
}
