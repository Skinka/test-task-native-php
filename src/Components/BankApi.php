<?php


namespace App\Components;

use App\Events\MoneyDeliveredEvent;
use App\System\App;

/**
 * Class BankApi
 * @package App\Components
 */
class BankApi implements BankInterface
{

    /**
     * Отправка денежного приза на банковский счет пользователю
     *
     * @param $orderId
     * @param $amount
     * @param $bankData
     *
     * @return bool
     */
    public function sendTransaction($orderId, $amount, $bankData): bool
    {
        $complete = $this->useApi($orderId, $amount, $bankData);
        if ($complete) {
            $this->approveTransaction($orderId);
            return true;
        }
        // @TODO логируем ошибку операции
        return false;
    }

    /**
     * @param $orderId
     */
    protected function approveTransaction($orderId)
    {
        $dispatcher = App::$app->getDispatcher();
        $dispatcher->dispatch(new MoneyDeliveredEvent($orderId), MoneyDeliveredEvent::NAME);
    }

    /**
     * @param $orderId
     * @param $amount
     * @param $bankData
     *
     * @return bool
     */
    protected function useApi($orderId, $amount, $bankData): bool
    {
        // @TODO отправка денежного приза на счет в банке
        return true;
    }
}
