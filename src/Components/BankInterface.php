<?php


namespace App\Components;

/**
 * Interface BankInterface
 * @package App\Components
 */
interface BankInterface
{
    public function sendTransaction($orderId, $amount, $bankData): bool;
}
