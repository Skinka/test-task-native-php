<?php


namespace App\Repositories;

use App\Enums\PrizesEnum;
use App\Exceptions\MoneyException;
use App\System\App;
use PDO;

/**
 * Class PrizeRepository
 * @package App\Repositories
 */
class PrizeRepository extends BaseRepository
{
    private MoneyRepository $moneyRepo;
    private ThingRepository $thingRepo;

    public function __construct(MoneyRepository $moneyRepo, ThingRepository $thingRepo)
    {
        $this->moneyRepo = $moneyRepo;
        $this->thingRepo = $thingRepo;
    }

    /**
     * Добавляет денежный приз пользователю
     *
     * @param  int    $user_id
     * @param  float  $amount
     *
     * @return void
     * @throws MoneyException
     */
    public function setMoneyToUser(int $user_id, float $amount): void
    {
        $query = App::$app->db
            ->prepare('INSERT INTO `user_money_prizes` (`user_id`, `amount`) VALUE (:user, :amount)');
        $query->bindParam(':user', $user_id);
        $query->bindParam(':amount', $amount);
        $query->execute();
        $this->moneyRepo->decrease($amount);
    }

    /**
     * Добавляет бонусный приз пользователю
     *
     * @param  int    $user_id
     * @param  float  $amount
     *
     * @return void
     */
    public function setBonusToUser(int $user_id, float $amount): void
    {
        $query = App::$app->db
            ->prepare('INSERT INTO `user_bonus_prizes` (`user_id`, `amount`) VALUE (:user, :amount)');
        $query->bindParam(':user', $user_id);
        $query->bindParam(':amount', $amount);
        $query->execute();
    }

    /**
     * Добавляет приз пользователю
     *
     * @param  int  $user_id
     * @param  int  $thing_id
     *
     * @return void
     */
    public function setThingToUser(int $user_id, int $thing_id): void
    {
        $query = App::$app->db
            ->prepare('INSERT INTO `user_thing_prizes` (`user_id`, `thing_id`) VALUE (:user, :thing)');
        $query->bindParam(':user', $user_id);
        $query->bindParam(':thing', $thing_id);
        $query->execute();
        $this->thingRepo->decrease($thing_id, 1);
    }

    /**
     * Возвращает денежный приз
     *
     * @param  int  $id
     *
     * @return array|null
     */
    public function getMoneyPrize(int $id): ?array
    {
        $query = App::$app->db
            ->prepare(
                'SELECT id, user_id, amount, delivered_at, canceled_at FROM `user_money_prizes` WHERE `id` = :id'
            );
        $query->bindParam(':id', $id);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Возвращает денежный бонус
     *
     * @param  int  $id
     *
     * @return array|null
     */
    public function getBonusPrize(int $id): ?array
    {
        $query = App::$app->db
            ->prepare(
                'SELECT id, user_id, amount, delivered_at, canceled_at FROM `user_bonus_prizes` WHERE `id` = :id'
            );
        $query->bindParam(':id', $id);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Возвращает предметный приз
     *
     * @param  int  $id
     *
     * @return array|null
     */
    public function getThingPrize(int $id): ?array
    {
        $query = App::$app->db
            ->prepare(
                'SELECT id, user_id, thing_id, delivered_at, canceled_at FROM `user_thing_prizes` WHERE `id` = :id'
            );
        $query->bindParam(':id', $id);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Возвращает приз
     *
     * @param  string  $type
     * @param  int     $id
     *
     * @return array|null
     */
    public function getPrize(string $type, int $id): ?array
    {
        switch ($type) {
            case PrizesEnum::BONUS:
                return $this->getBonusPrize($id);
            case PrizesEnum::MONEY:
                return $this->getMoneyPrize($id);
            case PrizesEnum::THING:
                return $this->getThingPrize($id);
        }
        return null;
    }

    /**
     * Отказывается от приза
     *
     * @param  string  $type
     * @param  int     $id
     *
     * @return bool
     */
    public function refuse(string $type, int $id): bool
    {
        $query = App::$app->db
            ->prepare("UPDATE `user_{$type}_prizes` set `canceled_at` = now() WHERE `id` = :id");
        $query->bindParam(':id', $id);
        $query->execute();
        return $query->rowCount() > 0;
    }

    /**
     * Удаляет денежный приз
     *
     * @param  int  $id
     *
     * @return bool
     */
    public function dropMoney(int $id): bool
    {
        $query = App::$app->db
            ->prepare("DELETE FROM `user_money_prizes` WHERE `id` = :id");
        $query->bindParam(':id', $id);
        $query->execute();
        return $query->rowCount() > 0;
    }

    /**
     * Возвращает не отправленные денежные призы для отправки
     *
     * @param  int  $lastId
     * @param  int  $amount
     *
     * @return array
     */
    public function getNotDeliveredMoney(int $lastId, int $amount): array
    {
        $query = App::$app->db->prepare(
            <<<'SQL'
                SELECT `mp`.`id`, `mp`.`amount`, `u`.`bank_data`
                FROM `user_money_prizes` mp
                LEFT JOIN `users` u on `mp`.`user_id` = `u`.`id`
                WHERE 
                      `canceled_at` is null and 
                      `converted_at` is null and
                      `delivered_at` is null and
                      `mp`.`id` > :lastId
                ORDER BY `id` 
                LIMIT :batch
            SQL
        );
        $query->bindParam(':lastId', $lastId, PDO::PARAM_INT);
        $query->bindParam(':batch', $amount, PDO::PARAM_INT);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Возвращает количество не отправленных денежных призов
     * @return int
     */
    public function getCountNotDeliveredMoney(): int
    {
        $query = App::$app->db->prepare(
            <<<'SQL'
                SELECT COUNT(*) cnt
                FROM `user_money_prizes`
                WHERE 
                      `canceled_at` is null and 
                      `converted_at` is null and
                      `delivered_at` is null
            SQL
        );
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);
        return (int)$data['cnt'];
    }

    /**
     * Устанавливает признак зачисления денежных средств на счет пользователя
     *
     * @param  int  $id
     */
    public function deliveryMoney(int $id)
    {
        $query = App::$app->db->prepare(
            <<<'SQL'
                UPDATE `user_money_prizes` SET `delivered_at` = NOW() WHERE `id` = :id
            SQL
        );
        $query->bindParam(':id', $id, PDO::PARAM_INT);
        $query->execute();
    }
}
