<?php


namespace App\Repositories;

use App\System\App;
use PDO;

/**
 * Class ThingRepository
 * @package App\Repositories
 */
class ThingRepository extends BaseRepository
{
    /**
     * Возвращает доступные призы
     * @return array
     */
    public function getAvailable(): array
    {
        $query = App::$app->db
            ->prepare('SELECT id, name FROM `things` WHERE `amount` > 0');
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Уменьшает хранилище
     *
     * @param  int  $thing_id
     * @param  int  $amount
     *
     * @return bool
     */
    public function decrease(int $thing_id, int $amount): bool
    {
        return $this->updateAmount($thing_id, $amount * -1);
    }

    /**
     * Производит возврат денежных средств в хранилище
     *
     * @param  int  $thing_id
     * @param  int  $amount
     *
     * @return bool
     */
    public function increase(int $thing_id, int $amount): bool
    {
        return $this->updateAmount($thing_id, $amount);
    }

    /**
     * Обновляет доступное количество предмета
     *
     * @param  int  $thing_id
     * @param  int  $amount
     *
     * @return bool
     */
    protected function updateAmount(int $thing_id, int $amount): bool
    {
        $query = App::$app->db
            ->prepare('UPDATE `things` SET amount = amount + :amount WHERE `id` = :id');
        $query->bindParam(':amount', $amount, PDO::PARAM_INT);
        $query->bindParam(':id', $thing_id, PDO::PARAM_INT);
        return $query->execute();
    }
}
