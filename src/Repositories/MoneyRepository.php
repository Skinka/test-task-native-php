<?php


namespace App\Repositories;

use App\Exceptions\MoneyException;
use App\System\App;
use PDO;

/**
 * Class MoneyRepository
 * @package App\Repositories
 */
class MoneyRepository extends BaseRepository
{
    /**
     * Возвращает доступное денежное вознаграждение
     * @return float
     */
    public function getAvailable(): float
    {
        $query = App::$app->db
            ->prepare('SELECT SUM(amount) max FROM `money`');
        $query->execute();
        $data = $query->fetch(PDO::FETCH_ASSOC);
        return (float)$data['max'];
    }

    /**
     * Уменьшает хранилище
     *
     * @param  float  $amount
     *
     * @throws MoneyException
     */
    public function decrease(float $amount): void
    {
        do {
            $data = $this->getBigGlobalAmount();
            if (!$data) {
                throw new MoneyException('Нехватка денег на глобальном балансе');
            }
            $decrease = $amount;
            if ($data['amount'] < $amount) {
                $decrease = $data['amount'];
            }
            $this->updateGlobalAmount($data['id'], $decrease);
            $amount -= $decrease;
        } while ($amount != 0);
    }

    /**
     * Производит возврат денежных средств в хранилище
     *
     * @param  float  $amount
     *
     * @return bool
     */
    public function increase(float $amount): bool
    {
        return $this->insertGlobalAmount($amount);
    }

    /**
     * Возвращает запись о максимально заполненном глобальном счете
     * @return array
     */
    protected function getBigGlobalAmount(): array
    {
        $query = App::$app->db
            ->prepare('SELECT id, amount FROM `money` WHERE `amount` > 0 ORDER BY amount DESC LIMIT 1');
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Обновляет глобальный счет
     *
     * @param  int    $id
     * @param  float  $amount
     *
     * @return bool
     */
    protected function updateGlobalAmount(int $id, float $amount): bool
    {
        $query = App::$app->db
            ->prepare('UPDATE `money` SET amount = amount - :amount WHERE `id` = :id');
        $query->bindParam(':amount', $amount, PDO::PARAM_INT);
        $query->bindParam(':id', $id, PDO::PARAM_INT);
        return $query->execute();
    }

    /**
     * Пополняет баланс глобального счета
     *
     * @param  float  $amount
     *
     * @return bool
     */
    protected function insertGlobalAmount(float $amount): bool
    {
        $query = App::$app->db
            ->prepare('INSERT INTO `money` (`amount`) value (:amount)');
        $query->bindParam(':amount', $amount, PDO::PARAM_INT);
        return $query->execute();
    }
}
