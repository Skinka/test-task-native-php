<?php


namespace App\Repositories;


use App\Entities\User;
use App\System\App;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository extends BaseRepository
{
    /**
     * Сохраняет пользователя в базе данных
     *
     * @param  User  $user
     *
     * @return bool
     */
    public function store(User $user): bool
    {
        $query = App::$app->db
            ->prepare(
                <<<'SQL'
            INSERT INTO `users` 
                (`username`, `password`, `address`, `bank_data`) 
                value 
                (:username, :password, :address, :bank_data)
            SQL
            );
        $query->bindParam(':username', $user->username);
        $query->bindParam(':password', $user->hashedPassword);
        $query->bindParam(':address', $user->address);
        $query->bindParam(':bank_data', $user->bank_data);
        return $query->execute();
    }
}
