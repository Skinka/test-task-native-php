<?php


namespace App\Services;

use App\Enums\PrizesEnum;
use App\Exceptions\DrawingException;
use App\Exceptions\MoneyException;
use App\Repositories\MoneyRepository;
use App\Repositories\PrizeRepository;
use App\Repositories\ThingRepository;
use App\System\App;

/**
 * Class DrawingService
 * @package App\Services
 */
class DrawingService
{
    private MoneyRepository $moneyRepo;
    private PrizeRepository $prizeRepo;
    private ThingRepository $thingRepo;

    public function __construct(PrizeRepository $prizeRepo, MoneyRepository $moneyRepo, ThingRepository $thingRepo)
    {
        $this->moneyRepo = $moneyRepo;
        $this->prizeRepo = $prizeRepo;
        $this->thingRepo = $thingRepo;
    }

    /**
     * Разыгрывает приз для пользователя
     * @return string
     * @throws DrawingException|MoneyException
     */
    public function getPrize(): string
    {
        do {
            $isSelect = false;
            $prize = $this->getRandomPrizeType();
            $min = App::$app->config['prizes'][$prize]['min'];
            $max = App::$app->config['prizes'][$prize]['max'];
            switch ($prize) {
                case PrizesEnum::MONEY:
                    $available = $this->moneyRepo->getAvailable();
                    if ($min > $available) {
                        break;
                    }
                    $max = $max > $available ? $available : $max;

                    $amount = rand($min, $max);
                    $this->prizeRepo->setMoneyToUser($this->getAuthUserId(), $amount);
                    $isSelect = true;
                    break;
                case PrizesEnum::BONUS:
                    $amount = rand($min, $max);

                    $this->prizeRepo->setBonusToUser($this->getAuthUserId(), $amount);
                    $isSelect = true;
                    break;
                case PrizesEnum::THING:
                    $things = $this->thingRepo->getAvailable();
                    if (!$things) {
                        break;
                    }
                    $thing = $things[array_rand($things)];
                    $this->prizeRepo->setThingToUser($this->getAuthUserId(), $thing['id']);
                    $isSelect = true;
                    break;
                default:
                    throw new DrawingException(sprintf('Неизвестный тип приза: %s', $prize));
            }
        } while (!$isSelect);

        return $prize;
    }

    protected function getAuthUserId(): int
    {
        return App::$app->auth->user()->getAuthId();
    }

    /**
     * Возвращает произвольный тип приза
     * @return string
     */
    protected function getRandomPrizeType(): string
    {
        $prizes = array_keys(App::$app->config['prizes']);
        return $prizes[array_rand($prizes)];
    }
}
