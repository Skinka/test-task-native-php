<?php


namespace App\Services;

use App\Enums\PrizesEnum;
use App\Exceptions\PrizeException;
use App\Repositories\MoneyRepository;
use App\Repositories\PrizeRepository;
use App\Repositories\ThingRepository;
use App\System\App;

/**
 * Class PrizesService
 * @package App\Services
 */
class UserService
{
    private PrizeRepository $prizeRepo;
    private MoneyRepository $moneyRepo;
    private ThingRepository $thingRepo;

    public function __construct(PrizeRepository $prizeRepo, MoneyRepository $moneyRepo, ThingRepository $thingRepo)
    {
        $this->prizeRepo = $prizeRepo;
        $this->moneyRepo = $moneyRepo;
        $this->thingRepo = $thingRepo;
    }

    /**
     * Пользователь отказывается от приза
     *
     * @param  int     $user_id
     * @param  string  $type
     * @param  int     $id
     *
     * @return bool
     * @throws PrizeException
     */
    public function refuse(int $user_id, string $type, int $id): bool
    {
        $prize = $this->prizeRepo->getPrize($type, $id);
        if (!$prize) {
            throw new PrizeException('Приз не найден');
        }
        if ($prize['user_id'] != $user_id) {
            throw new PrizeException('Это чужой приз');
        }
        if ($prize['canceled_at']) {
            throw new PrizeException('Приз уже вернули');
        }
        if ($prize['delivered_at']) {
            throw new PrizeException('Приз уже получен');
        }
        $refused = $this->prizeRepo->refuse($type, $id);
        if (!$refused) {
            return false;
        }
        switch ($type) {
            case PrizesEnum::MONEY:
                $this->moneyRepo->increase($prize['amount']);
                break;
            case PrizesEnum::THING:
                $this->thingRepo->increase($prize['thing_id'], 1);
                break;
        }
        return true;
    }

    /**
     * Конвертирует денежный приз в бонусы
     *
     * @param  int  $user_id
     * @param  int  $id
     *
     * @throws PrizeException
     */
    public function convert(int $user_id, int $id): void
    {
        $prize = $this->prizeRepo->getMoneyPrize($id);
        if (!$prize) {
            throw new PrizeException('Приз не найден');
        }
        if ($prize['user_id'] != $user_id) {
            throw new PrizeException('Это чужой приз');
        }
        if ($prize['canceled_at']) {
            throw new PrizeException('Приз уже вернули');
        }
        if ($prize['delivered_at']) {
            throw new PrizeException('Приз уже получен');
        }

        $this->prizeRepo->dropMoney($id);

        $this->moneyRepo->increase($prize['amount']);
        $this->prizeRepo->setBonusToUser(
            $user_id,
            $prize['amount'] * App::$app->config['prizes'][PrizesEnum::BONUS]['convert']
        );
    }
}
