<?php


namespace App\Controllers;

use App\Enums\PrizesEnum;
use App\Exceptions\PrizeException;
use App\Services\UserService;
use App\System\App;
use Exception;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserController
 * @package App\Controllers
 */
class UserController extends BaseController
{
    protected UserService $service;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->service = App::$app->di->get(UserService::class);
    }

    /**
     * Отказывается от денег
     *
     * @param $id
     *
     * @return Response
     * @throws PrizeException
     */
    public function moneyRefuse($id): Response
    {
        $refused = $this->service->refuse(App::$app->auth->user()->getAuthId(), PrizesEnum::MONEY, $id);
        return new Response(json_encode(['success' => $refused]), 200, ['Content-Type' => 'application/json']);
    }

    /**
     * Отказывается от бонуса
     *
     * @param $id
     *
     * @return Response
     * @throws PrizeException
     */
    public function bonusRefuse($id): Response
    {
        $refused = $this->service->refuse(App::$app->auth->user()->getAuthId(), PrizesEnum::BONUS, $id);
        return new Response(json_encode(['success' => $refused]), 200, ['Content-Type' => 'application/json']);
    }

    /**
     * Отказывается от предмета
     *
     * @param $id
     *
     * @return Response
     * @throws PrizeException
     */
    public function thingRefuse($id): Response
    {
        $refused = $this->service->refuse(App::$app->auth->user()->getAuthId(), PrizesEnum::THING, $id);
        return new Response(json_encode(['success' => $refused]), 200, ['Content-Type' => 'application/json']);
    }

    /**
     * Конвертирование денежного приза в бонусный
     *
     * @param  int  $id  идентификатор денежного приза
     *
     * @throws PrizeException
     */
    public function convert(int $id): Response
    {
        $this->service->convert(App::$app->auth->user()->getAuthId(), $id);
        return new Response(json_encode(['converted' => true]), 200, ['Content-Type' => 'application/json']);
    }
}
