<?php


namespace App\Controllers;

use App\System\App;
use Jasny\Auth\LoginException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AuthController
 * @package App\Controllers
 */
class AuthController extends BaseController
{
    /**
     * Авторизует пользователя
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function login(Request $request): Response
    {
        if (App::$app->auth->isLoggedIn()) {
            return new Response(
                json_encode(['error' => 'Пользователь авторизирован']),
                403,
                ['Content-Type' => 'application/json']
            );
        }
        $data = $request->request->all();
        try {
            App::$app->auth->login($data['username'], $data['password']);
        } catch (LoginException $e) {
            return new Response(
                json_encode(['error' => 'Не верный логин или пароль']),
                422,
                ['Content-Type' => 'application/json']
            );
        }
        return new Response('', 204, ['Content-Type' => 'application/json']);
    }
}
