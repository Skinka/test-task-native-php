<?php


namespace App\Controllers;

use App\Entities\User;
use App\Services\DrawingService;
use App\System\App;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Class DrawingController
 * @package App\Controllers
 */
class DrawingController extends BaseController
{
    /**
     * Разыгрывает приз для авторизированного пользователя
     * @return Response
     * @throws Exception
     */
    public function play(): Response
    {
        if (App::$app->auth->isLoggedOut()) {
            throw new UnauthorizedHttpException('Unauthorized');
        }

        $service = App::$app->di->get(DrawingService::class);
        $prize = $service->getPrize();

        return new Response(json_encode(['prize' => $prize]), 200, ['Content-Type' => 'application/json']);
    }

    /**
     * Разыгрывает приз для авторизированного пользователя
     * Метод сделан для AB нагрузочного тестирования ;)
     * @return Response
     * @throws Exception
     */
    public function playUser(): Response
    {
        $user = User::fromData(['id' => 1]);

        App::$app->auth->loginAs($user);
        $service = App::$app->di->get(DrawingService::class);
        $prize = $service->getPrize();

        return new Response(json_encode(['prize' => $prize]), 200, ['Content-Type' => 'application/json']);
    }
}
