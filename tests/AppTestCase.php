<?php


namespace Tests;

use App\Application;
use PHPUnit\Framework\TestCase;

/**
 * Class AppTestCase
 * @package Tests
 */
class AppTestCase extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $dotenv = new \Symfony\Component\Dotenv\Dotenv();
        $dotenv->usePutenv(true)->bootEnv(__DIR__.'/../.env');

        $conf = require(__DIR__.'/../config/app.php');

        $app = new Application($conf);
        $app->runTest();
    }
}
