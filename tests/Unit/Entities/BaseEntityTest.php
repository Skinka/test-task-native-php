<?php

namespace Entities;

use App\Entities\User;
use PHPUnit\Framework\TestCase;

class BaseEntityTest extends TestCase
{
    public function testFromData()
    {
        $userForm = User::fromData(['id' => 1, 'username' => 'username']);
        $user = new User();
        $user->id = 1;
        $user->username = 'username';
        $this->assertEquals($user, $userForm);
    }
}
