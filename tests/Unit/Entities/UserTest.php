<?php

namespace Entities;

use App\Entities\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    protected $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = new User();
        $this->user->id = 1;
        $this->user->setPassword('123456');
    }


    public function testGetAuthId()
    {
        $this->assertEquals(1, $this->user->getAuthId());
    }

    public function testVerifyPassword()
    {
        $this->assertTrue($this->user->verifyPassword('123456'));
        $this->assertFalse($this->user->verifyPassword('12345'));
    }

    public function testGetAuthChecksum()
    {
        $this->assertEquals(
            hash('sha256', $this->user->id.$this->user->hashedPassword),
            $this->user->getAuthChecksum()
        );
    }

    public function testRequiresMfa()
    {
        $this->assertFalse($this->user->requiresMfa());
    }

    public function testGetAuthRole()
    {
        $this->assertEquals(0, $this->user->getAuthRole(null));
    }

    public function testSetPassword()
    {
        $this->user->hashedPassword = '';
        $this->user->setPassword('12345');
        $this->assertNotEmpty($this->user->hashedPassword);
    }
}
