<?php

namespace Commands;

use App\Commands\UserAddCommand;
use App\Repositories\UserRepository;
use App\System\App;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;
use Tests\AppTestCase;

class UserAddCommandTest extends AppTestCase
{
    public function testExecute()
    {
        $command = $this->getMockBuilder(UserAddCommand::class)
            ->onlyMethods(['checkUnique'])
            ->getMock();

        $command->expects($this->exactly(3))->method('checkUnique')
            ->willReturn(false, true, true);

        $userRepo = $this->getMockBuilder(UserRepository::class)
            ->onlyMethods(['store'])
            ->getMock();
        $userRepo->expects($this->exactly(2))->method('store')->withAnyParameters()->willReturn(true, false);
        App::$app->di->set(UserRepository::class, $userRepo);

        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                'username' => 'user',
                'password' => '123456'
            ]
        );
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('User "user" is exist', $output);

        $commandTester->execute(
            [
                'username' => 'user',
                'password' => '123456'
            ]
        );
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('User Added', $output);

        $commandTester->execute(
            [
                'username' => 'user',
                'password' => '123456'
            ]
        );
        $output = $commandTester->getStatusCode();
        $this->assertEquals(Command::FAILURE, $output);
    }
}
