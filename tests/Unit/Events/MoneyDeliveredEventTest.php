<?php

namespace Events;

use App\Events\MoneyDeliveredEvent;
use Tests\AppTestCase;

class MoneyDeliveredEventTest extends AppTestCase
{

    public function testGetMoneyPrizeId()
    {
        $event = new MoneyDeliveredEvent(3);
        $this->assertEquals(3, $event->getMoneyPrizeId());
    }
}
