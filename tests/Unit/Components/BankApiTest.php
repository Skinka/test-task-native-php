<?php

namespace Components;

use App\Components\BankApi;
use App\Repositories\PrizeRepository;
use Tests\AppTestCase;

class BankApiTest extends AppTestCase
{
    public function testSendTransaction()
    {
        $prizeRepo = $this->createMock(PrizeRepository::class);
        $prizeRepo->method('deliveryMoney')->withAnyParameters();

        $service = $this->getMockBuilder(BankApi::class)
            ->onlyMethods(['approveTransaction', 'useApi'])
            ->getMock();
        $service->expects($this->any())->method('approveTransaction')->withAnyParameters();
        $service->expects($this->exactly(2))->method('useApi')->willReturn(true, false);

        $this->assertTrue($service->sendTransaction(1, 1, ''));
        $this->assertFalse($service->sendTransaction(1, 1, ''));
    }
}
