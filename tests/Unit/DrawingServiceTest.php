<?php

namespace Tests\Unit;

use App\Enums\PrizesEnum;
use App\Exceptions\DrawingException;
use App\Repositories\MoneyRepository;
use App\Repositories\PrizeRepository;
use App\Repositories\ThingRepository;
use App\Services\DrawingService;
use Tests\AppTestCase;

class DrawingServiceTest extends AppTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @throws \App\Exceptions\DrawingException
     * @throws \App\Exceptions\MoneyException
     */
    public function testGetPrize()
    {
        $prizeRepo = $this->createMock(PrizeRepository::class);
        $prizeRepo->method('setMoneyToUser')->withAnyParameters();
        $prizeRepo->method('setBonusToUser')->withAnyParameters();
        $prizeRepo->method('setThingToUser')->withAnyParameters();
        $moneyRepo = $this->createMock(MoneyRepository::class);
        $moneyRepo->method('getAvailable')->willReturn((float)1000);
        $moneyRepo->method('decrease');
        $thingRepo = $this->createMock(ThingRepository::class);
        $thingRepo->method('getAvailable')->willReturn([['id' => 1, 'name' => 'Test']]);
        $service = $this->getMockBuilder(DrawingService::class)
            ->setConstructorArgs([$prizeRepo, $moneyRepo, $thingRepo])
            ->onlyMethods(['getAuthUserId'])
            ->getMock();

        $service->expects($this->any())->method('getAuthUserId')->willReturn(1);
        $result = $service->getPrize();
        $this->assertContains(
            $result,
            [
                PrizesEnum::BONUS,
                PrizesEnum::MONEY,
                PrizesEnum::THING,
            ]
        );

        $service = $this->getMockBuilder(DrawingService::class)
            ->setConstructorArgs([$prizeRepo, $moneyRepo, $thingRepo])
            ->onlyMethods(['getAuthUserId', 'getRandomPrizeType'])
            ->getMock();
        $service->expects($this->any())->method('getAuthUserId')->willReturn(1);
        $service->expects($this->exactly(3))
            ->method('getRandomPrizeType')
            ->willReturn(
                PrizesEnum::BONUS,
                PrizesEnum::MONEY,
                PrizesEnum::THING,
            );
        $result = $service->getPrize();
        $this->assertEquals(PrizesEnum::BONUS, $result);
        $result = $service->getPrize();
        $this->assertEquals(PrizesEnum::MONEY, $result);
        $result = $service->getPrize();
        $this->assertEquals(PrizesEnum::THING, $result);
    }

    /**
     * @throws \App\Exceptions\DrawingException
     * @throws \App\Exceptions\MoneyException
     */
    public function testGetPrizeNotMoney()
    {
        $prizeRepo = $this->createMock(PrizeRepository::class);
        $prizeRepo->method('setMoneyToUser')->withAnyParameters();
        $prizeRepo->method('setBonusToUser')->withAnyParameters();
        $prizeRepo->method('setThingToUser')->withAnyParameters();
        $moneyRepo = $this->createMock(MoneyRepository::class);
        $moneyRepo->method('getAvailable')->willReturn((float)10);
        $moneyRepo->method('decrease');
        $thingRepo = $this->createMock(ThingRepository::class);
        $service = $this->getMockBuilder(DrawingService::class)
            ->setConstructorArgs([$prizeRepo, $moneyRepo, $thingRepo])
            ->onlyMethods(['getAuthUserId', 'getRandomPrizeType'])
            ->getMock();
        $service->expects($this->any())->method('getAuthUserId')->willReturn(1);
        $service->expects($this->exactly(2))->method('getRandomPrizeType')->willReturn(
            PrizesEnum::MONEY,
            PrizesEnum::BONUS,
        );
        $result = $service->getPrize();
        $this->assertEquals(PrizesEnum::BONUS, $result);
    }

    /**
     * @throws \App\Exceptions\DrawingException
     * @throws \App\Exceptions\MoneyException
     */
    public function testGetPrizeNotThing()
    {
        $prizeRepo = $this->createMock(PrizeRepository::class);
        $prizeRepo->method('setMoneyToUser')->withAnyParameters();
        $prizeRepo->method('setBonusToUser')->withAnyParameters();
        $prizeRepo->method('setThingToUser')->withAnyParameters();
        $moneyRepo = $this->createMock(MoneyRepository::class);
        $thingRepo = $this->createMock(ThingRepository::class);
        $thingRepo->method('getAvailable')->willReturn([]);
        $service = $this->getMockBuilder(DrawingService::class)
            ->setConstructorArgs([$prizeRepo, $moneyRepo, $thingRepo])
            ->onlyMethods(['getAuthUserId', 'getRandomPrizeType'])
            ->getMock();
        $service->expects($this->any())->method('getAuthUserId')->willReturn(1);
        $service->expects($this->exactly(2))->method('getRandomPrizeType')->willReturn(
            PrizesEnum::THING,
            PrizesEnum::BONUS,
        );
        $result = $service->getPrize();
        $this->assertEquals(PrizesEnum::BONUS, $result);
    }

    /**
     * @throws \App\Exceptions\DrawingException
     * @throws \App\Exceptions\MoneyException
     */
    public function testGetPrizeException()
    {
        $prizeRepo = $this->createMock(PrizeRepository::class);
        $prizeRepo->method('setMoneyToUser')->withAnyParameters();
        $prizeRepo->method('setBonusToUser')->withAnyParameters();
        $prizeRepo->method('setThingToUser')->withAnyParameters();
        $moneyRepo = $this->createMock(MoneyRepository::class);
        $thingRepo = $this->createMock(ThingRepository::class);
        $service = $this->getMockBuilder(DrawingService::class)
            ->setConstructorArgs([$prizeRepo, $moneyRepo, $thingRepo])
            ->onlyMethods(['getAuthUserId', 'getRandomPrizeType'])
            ->getMock();
        $service->expects($this->once())->method('getRandomPrizeType')->willReturn(
            'ERROR',
        );
        $this->expectException(DrawingException::class);
        $this->expectExceptionMessage('Неизвестный тип приза: ERROR');
        $service->getPrize();
    }
}
