<?php

use App\Enums\PrizesEnum;

return [
    'db'     => [
        'host'     => getenv('DB_HOST', 'localhost'),
        'user'     => getenv('DB_USERNAME', 'root'),
        'password' => getenv('DB_PASSWORD', ''),
        'database' => getenv('DB_DATABASE', 'default')
    ],
    'prizes' => [
        PrizesEnum::MONEY => [
            'min'     => 100,
            'max'     => 500,
            'convert' => 1.5
        ],
        PrizesEnum::BONUS => [
            'min' => 100,
            'max' => 500
        ],
        PrizesEnum::THING => [
            'min' => 1,
            'max' => 1
        ]
    ],
];
