# Пользователь

## Призы пользователя

Отказ от денежного приза

```
localhost:8080/user/refuse/money/{money_prize_id}
```

Отказ от бонусного приза

```
localhost:8080/user/refuse/bonus/{bonus_prize_id}
```

Отказ от приза предмета

```
localhost:8080/user/refuse/thing/{thing_prize_id}
```

Конвертация денежного приза в бонусный

```
localhost:8080/user/convert/{money_prize_id}
```